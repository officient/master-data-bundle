<?php

namespace Officient\MasterData;

use Officient\MasterData\Exception\MasterDataException;

/**
 * Interface ClientInterface
 * @package Officient\MasterData
 */
interface ClientInterface
{
    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';
    public const METHOD_PATCH = 'PATCH';
    public const METHOD_DELETE = 'DELETE';

    /**
     * @param string $query
     * @param null $data
     * @param string $method
     * @return Response
     * @throws MasterDataException
     */
    public function doRequest(string $query, $data = null, string $method = self::METHOD_GET): Response;
}