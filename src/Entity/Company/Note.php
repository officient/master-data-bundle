<?php

namespace Officient\MasterData\Entity\Company;

class Note implements \JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $note;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * @var \DateTime|null
     */
    private $deletedDatetime;

    /**
     * @var \DateTime|null
     */
    private $updatedDatetime;

    /**
     * @var \DateTime|null
     */
    private $createdDatetime;

    /**
     * @var string|null
     */
    private $userName;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Note
     */
    public function setId(?int $id): Note
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string|null $note
     * @return Note
     */
    public function setNote(?string $note): Note
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     * @return Note
     */
    public function setUserId(?int $userId): Note
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDatetime(): ?\DateTime
    {
        return $this->deletedDatetime;
    }

    /**
     * @param \DateTime|null $deletedDatetime
     * @return Note
     */
    public function setDeletedDatetime(?\DateTime $deletedDatetime): Note
    {
        $this->deletedDatetime = $deletedDatetime;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedDatetime(): ?\DateTime
    {
        return $this->updatedDatetime;
    }

    /**
     * @param \DateTime|null $updatedDatetime
     * @return Note
     */
    public function setUpdatedDatetime(?\DateTime $updatedDatetime): Note
    {
        $this->updatedDatetime = $updatedDatetime;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDatetime(): ?\DateTime
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTime|null $createdDatetime
     * @return Note
     */
    public function setCreatedDatetime(?\DateTime $createdDatetime): Note
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserName(): ?string
    {
        return $this->userName;
    }

    /**
     * @param string|null $userName
     * @return Note
     */
    public function setUserName(?string $userName): Note
    {
        $this->userName = $userName;
        return $this;
    }
}