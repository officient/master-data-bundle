<?php

namespace Officient\MasterData\Entity\Company;

/**
 * Class Config
 * @package Officient\MasterData\Entity\Company
 */
class Config implements \JsonSerializable
{
    /**
     * @var int
     */
    private $companyId;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @var string
     */
    private $value;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     */
    public function setCompanyId(int $companyId): void
    {
        $this->companyId = $companyId;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return Config
     */
    public function setIdentifier(string $identifier): Config
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Config
     */
    public function setValue(string $value): Config
    {
        $this->value = $value;
        return $this;
    }
}