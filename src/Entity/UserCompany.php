<?php

namespace Officient\MasterData\Entity;

/**
 * Class UserCompany
 * @package Officient\MasterData\Entity
 */
class UserCompany implements \JsonSerializable
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $companyId;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var array
     */
    private $roles = [];

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCompanyId(): ?int
    {
        return $this->companyId;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return $this
     */
    public function setCompany(Company $company): self
    {
        $this->company = $company;
        $this->companyId = $company->getId();

        return $this;
    }

    /**
     * @return array|null
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getJsonRoles(): ?string
    {
        if(is_null($this->roles)) {
            return null;
        }
        return json_encode($this->roles);
    }

    /**
     * @param string|null $jsonRoles
     * @return $this
     */
    public function setJsonRoles(?string $jsonRoles): self
    {
        if(is_null($jsonRoles)) {
            $this->roles = array();
        } else {
            $this->roles = json_decode($jsonRoles, true);
        }

        return $this;
    }
}
