<?php

namespace Officient\MasterData\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 * @package Officient\MasterData\Entity
 */
class User implements UserInterface, \JsonSerializable
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_SYS_ADMIN = 'ROLE_SYS_ADMIN';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $firstName;

    /**
     * @var string|null
     */
    private $lastName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var bool
     */
    private $emailConfirmed;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var \DateTimeInterface|null
     */
    private $lastLoginDatetime;

    /**
     * @var \DateTimeInterface
     */
    private $createdDatetime;

    /**
     * @var \DateTimeInterface
     */
    private $updatedDatetime;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return bool
     */
    public function isSysAdmin(): bool
    {
        return in_array(self::ROLE_SYS_ADMIN, $this->roles);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return $this
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return $this
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return $this
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array|null $roles
     * @return $this
     */
    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getJsonRoles(): ?string
    {
        if(is_null($this->roles)) {
            return null;
        }
        return json_encode($this->roles);
    }

    /**
     * @param string|null $jsonRoles
     * @return $this
     */
    public function setJsonRoles(?string $jsonRoles): self
    {
        if(is_null($jsonRoles)) {
            $this->roles = array();
        } else {
            $this->roles = json_decode($jsonRoles, true);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailConfirmed(): bool
    {
        return $this->emailConfirmed;
    }

    /**
     * @param bool $emailConfirmed
     * @return User
     */
    public function setEmailConfirmed(bool $emailConfirmed): User
    {
        $this->emailConfirmed = $emailConfirmed;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return User
     */
    public function setActive(bool $active): User
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return User
     */
    public function setPhone(?string $phone): User
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPassword(): ?string
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getUsername(): ?string
    {
        return $this->getEmail();
    }

    public function getUserIdentifier(): ?string
    {
        return $this->getUsername();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // Not needed
    }

    public function getLastLoginDatetime(): ?\DateTimeInterface
    {
        return $this->lastLoginDatetime;
    }

    public function setLastLoginDatetime(?\DateTimeInterface $lastLoginDatetime): self
    {
        $this->lastLoginDatetime = $lastLoginDatetime;

        return $this;
    }

    public function getCreatedDatetime(): \DateTimeInterface
    {
        return $this->createdDatetime;
    }

    public function setCreatedDatetime(\DateTimeInterface $createdDatetime): self
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }

    public function getUpdatedDatetime(): \DateTimeInterface
    {
        return $this->updatedDatetime;
    }

    public function setUpdatedDatetime(\DateTimeInterface $updatedDatetime): self
    {
        $this->updatedDatetime = $updatedDatetime;
        return $this;
    }
}
