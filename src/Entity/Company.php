<?php

namespace Officient\MasterData\Entity;

/**
 * Class Company
 * @package Officient\MasterData\Entity
 */
class Company implements \JsonSerializable
{
    const INVOICING_DISPATCH_TYPE_EMAIL         = 'email';
    const INVOICING_DISPATCH_TYPE_E_INVOICE     = 'e-invoice';
    const INVOICING_DISPATCH_TYPES              = [
        self::INVOICING_DISPATCH_TYPE_EMAIL,
        self::INVOICING_DISPATCH_TYPE_E_INVOICE
    ];

    /**
     * @var int
     */
    private $id;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var int|null
     */
    private $parentId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $organizationNumber;

    /**
     * @var string
     */
    private $organizationNumberType;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $invoicingEmail;

    /**
     * @var string
     */
    private $invoicingLanguage;

    /**
     * @var string
     */
    private $invoicingDispatchType;

    /**
     * @var string
     */
    private $errorEmail;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $buyerReference;

    /**
     * @var string
     */
    private $sellerReference;

    /**
     * @var string
     */
    private $orderReference;

    /**
     * @var int
     */
    private $paymentTerms;

    /**
     * @var bool
     */
    private $hasUsers;

    /**
     * @var \DateTimeInterface|null
     */
    private $freeTrialDatetime;

    /**
     * @var string|null
     */
    private $endpointId;

    /**
     * @var string|null
     */
    private $endpointIdType;

    /**
     * @var \DateTimeInterface
     */
    private $createdDatetime;

    /**
     * @var \DateTimeInterface
     */
    private $updatedDatetime;

    /**
     * @var string|null
     */
    private $activeSubscriptionProductName;

    /**
     * @var string|null
     */
    private $promoCode;

    /**
     * @var bool|null
     */
    private $isParent;

    /**
     * @var bool|null
     */
    private $isChild;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Company
     */
    public function setActive(bool $active): Company
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     * @return Company
     */
    public function setParentId(?int $parentId): Company
    {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrganizationNumber(): ?string
    {
        return $this->organizationNumber;
    }

    /**
     * @param string|null $organizationNumber
     * @return Company
     */
    public function setOrganizationNumber(?string $organizationNumber): self
    {
        $this->organizationNumber = $organizationNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrganizationNumberType(): ?string
    {
        return $this->organizationNumberType;
    }

    /**
     * @param string|null $organizationNumberType
     * @return Company
     */
    public function setOrganizationNumberType(?string $organizationNumberType): self
    {
        $this->organizationNumberType = $organizationNumberType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return Company
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @param string|null $zip
     * @return Company
     */
    public function setZip(?string $zip): self
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return Company
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return Company
     */
    public function setCountry(?string $country): self
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Company
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvoicingEmail(): string
    {
        return $this->invoicingEmail;
    }

    /**
     * @param string $invoicingEmail
     * @return Company
     */
    public function setInvoicingEmail(string $invoicingEmail): self
    {
        $this->invoicingEmail = $invoicingEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvoicingLanguage(): string
    {
        return $this->invoicingLanguage;
    }

    /**
     * @param string $invoicingLanguage
     * @return Company
     */
    public function setInvoicingLanguage(string $invoicingLanguage): self
    {
        $this->invoicingLanguage = $invoicingLanguage;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvoicingDispatchType(): string
    {
        return $this->invoicingDispatchType;
    }

    /**
     * @param string $invoicingDispatchType
     * @return Company
     */
    public function setInvoicingDispatchType(string $invoicingDispatchType): Company
    {
        $this->invoicingDispatchType = $invoicingDispatchType;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorEmail(): ?string
    {
        return $this->errorEmail;
    }

    /**
     * @param string $errorEmail
     * @return Company
     */
    public function setErrorEmail(?string $errorEmail): Company
    {
        $this->errorEmail = $errorEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Company
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBuyerReference(): ?string
    {
        return $this->buyerReference;
    }

    /**
     * @param string|null $buyerReference
     * @return Company
     */
    public function setBuyerReference(?string $buyerReference): self
    {
        $this->buyerReference = $buyerReference;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSellerReference(): ?string
    {
        return $this->sellerReference;
    }

    /**
     * @param string|null $sellerReference
     * @return Company
     */
    public function setSellerReference(?string $sellerReference): self
    {
        $this->sellerReference = $sellerReference;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderReference(): ?string
    {
        return $this->orderReference;
    }

    /**
     * @param string $orderReference
     * @return Company
     */
    public function setOrderReference(?string $orderReference): Company
    {
        $this->orderReference = $orderReference;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPaymentTerms(): ?int
    {
        return $this->paymentTerms;
    }

    /**
     * @param int|null $paymentTerms
     * @return Company
     */
    public function setPaymentTerms(?int $paymentTerms): self
    {
        $this->paymentTerms = $paymentTerms;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasUsers(): bool
    {
        return $this->hasUsers;
    }

    /**
     * @param bool $hasUsers
     * @return Company
     */
    public function setHasUsers(bool $hasUsers): self
    {
        $this->hasUsers = $hasUsers;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getFreeTrialDatetime(): ?\DateTimeInterface
    {
        return $this->freeTrialDatetime;
    }

    /**
     * @param \DateTimeInterface|null $freeTrialDatetime
     * @return $this
     */
    public function setFreeTrialDatetime(?\DateTimeInterface $freeTrialDatetime): self
    {
        $this->freeTrialDatetime = $freeTrialDatetime;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEndpointId(): ?string
    {
        return $this->endpointId;
    }

    /**
     * @param string|null $endpointId
     * @return Company
     */
    public function setEndpointId(?string $endpointId): self
    {
        $this->endpointId = $endpointId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEndpointIdType(): ?string
    {
        return $this->endpointIdType;
    }

    /**
     * @param string|null $endpointIdType
     * @return Company
     */
    public function setEndpointIdType(?string $endpointIdType): self
    {
        $this->endpointIdType = $endpointIdType;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDatetime(): \DateTimeInterface
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTimeInterface $createdDatetime
     * @return $this
     */
    public function setCreatedDatetime(\DateTimeInterface $createdDatetime): self
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUpdatedDatetime(): \DateTimeInterface
    {
        return $this->updatedDatetime;
    }

    /**
     * @param \DateTimeInterface $updatedDatetime
     * @return $this
     */
    public function setUpdatedDatetime(\DateTimeInterface $updatedDatetime): self
    {
        $this->updatedDatetime = $updatedDatetime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getActiveSubscriptionProductName(): ?string
    {
        return $this->activeSubscriptionProductName;
    }

    /**
     * @param string|null $activeSubscriptionProductName
     * @return $this
     */
    public function setActiveSubscriptionProductName(?string $activeSubscriptionProductName): self
    {
        $this->activeSubscriptionProductName = $activeSubscriptionProductName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPromoCode(): ?string
    {
        return $this->promoCode;
    }

    /**
     * @param string|null $promoCode
     * @return $this
     */
    public function setPromoCode(?string $promoCode): self
    {
        $this->promoCode = $promoCode;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isParent(): ?bool
    {
        return $this->isParent;
    }

    /**
     * @param bool|null $isParent
     * @return Company
     */
    public function setIsParent(?bool $isParent): Company
    {
        $this->isParent = $isParent;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isChild(): ?bool
    {
        return $this->isChild;
    }

    /**
     * @param bool|null $isChild
     * @return Company
     */
    public function setIsChild(?bool $isChild): Company
    {
        $this->isChild = $isChild;
        return $this;
    }
}
