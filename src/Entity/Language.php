<?php

namespace Officient\MasterData\Entity;

/**
 * Class Language
 * @package Officient\MasterData\Entity
 */
class Language implements \JsonSerializable
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $translationKey;

    /**
     * @var string
     */
    private $iso6391;

    /**
     * @var string
     */
    private $iso6392;

    /**
     * @var string
     */
    private $tld;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Language
     */
    public function setId(int $id): Language
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Language
     */
    public function setName(string $name): Language
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getTranslationKey(): string
    {
        return $this->translationKey;
    }

    /**
     * @param string $translationKey
     * @return Language
     */
    public function setTranslationKey(string $translationKey): Language
    {
        $this->translationKey = $translationKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getIso6391(): string
    {
        return $this->iso6391;
    }

    /**
     * @param string $iso6391
     * @return Language
     */
    public function setIso6391(string $iso6391): Language
    {
        $this->iso6391 = $iso6391;
        return $this;
    }

    /**
     * @return string
     */
    public function getIso6392(): string
    {
        return $this->iso6392;
    }

    /**
     * @param string $iso6392
     * @return Language
     */
    public function setIso6392(string $iso6392): Language
    {
        $this->iso6392 = $iso6392;
        return $this;
    }

    public function getTld(): string
    {
        return $this->tld;
    }

    public function setTld(string $tld): Language
    {
        $this->tld = $tld;
        return $this;
    }
}