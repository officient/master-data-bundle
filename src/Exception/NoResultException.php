<?php

namespace Officient\MasterData\Exception;

/**
 * Class NoResultException
 * @package Officient\MasterData\Exception
 */
class NoResultException extends MasterDataException
{

}