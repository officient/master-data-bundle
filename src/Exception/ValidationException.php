<?php

namespace Officient\MasterData\Exception;

/**
 * Class ValidationException
 * @package Officient\MasterData\Exception
 */
class ValidationException extends MasterDataException
{

}