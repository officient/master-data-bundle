<?php

namespace Officient\MasterData\Exception;

/**
 * Class NonUniqueResultException
 * @package Officient\MasterData\Exception
 */
class NonUniqueResultException extends MasterDataException
{

}