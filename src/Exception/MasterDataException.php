<?php

namespace Officient\MasterData\Exception;

use Officient\MasterData\Response;
use Throwable;

/**
 * Class MasterDataException
 * @package Officient\MasterData\Exception
 */
class MasterDataException extends \Exception
{
    /**
     * @var null|Response
     */
    private $response;

    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null, Response $response = null)
    {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    /**
     * @return Response|null
     */
    public function getResponse(): ?Response
    {
        return $this->response;
    }
}