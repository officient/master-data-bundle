<?php

namespace Officient\MasterData\Exception;

/**
 * Class AuthenticationFailedException
 * @package Officient\MasterData\Exception
 */
class AuthenticationFailedException extends MasterDataException
{

}