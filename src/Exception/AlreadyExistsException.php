<?php

namespace Officient\MasterData\Exception;

/**
 * Class AlreadyExistsException
 * @package Officient\MasterData\Exception
 */
class AlreadyExistsException extends MasterDataException
{

}