<?php

namespace Officient\MasterData\Exception;

/**
 * Class NotFoundException
 * @package Officient\MasterData\Exception
 */
class NotFoundException extends MasterDataException
{

}