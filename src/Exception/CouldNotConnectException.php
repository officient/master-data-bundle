<?php

namespace Officient\MasterData\Exception;

/**
 * Class CouldNotConnectException
 * @package Officient\MasterData\Exception
 */
class CouldNotConnectException extends MasterDataException
{

}