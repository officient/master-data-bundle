<?php

namespace Officient\MasterData;

use Officient\MasterData\Exception\CouldNotConnectException;

/**
 * Class Client
 *
 * This class handles all communication with Master Data
 *
 * @package Officient\MasterData
 */
class Client implements ClientInterface
{
    /**
     * @var string|null
     */
    private $host;

    /**
     * @var int|null
     */
    private $port;

    /**
     * Client constructor.
     * @param string|null $host
     * @param int|null $port
     */
    public function __construct(?string $host, ?int $port)
    {
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * @inheritDoc
     */
    public function doRequest(string $query, $data = null, string $method = self::METHOD_GET): Response
    {
        if(empty($this->host)) {
            throw new CouldNotConnectException("Host is missing from config");
        }
        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, $this->getHost()."api/v1/".$query);
        if(!is_null($this->port)) {
            curl_setopt($handle, CURLOPT_PORT, $this->getPort());
        }
        curl_setopt($handle, CURLOPT_TIMEOUT, 30);
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        if($method !== self::METHOD_GET) {
            if(!is_null($data)) {
                $postFields = json_encode($data);
                curl_setopt($handle, CURLOPT_POSTFIELDS, $postFields);
            }
        }

        $content = curl_exec($handle);
        curl_close($handle);

        if(!$content) {
            throw new CouldNotConnectException("Could not connect to master data");
        }

        return new Response($content);
    }

    private function getHost()
    {
        $host = $this->host;
        if(substr($host, -1) !== '/') {
            $host .= '/';
        }
        return $host;
    }

    private function getPort()
    {
        return $this->port;
    }
}