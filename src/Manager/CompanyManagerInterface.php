<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Entity\Company;
use Officient\MasterData\Exception\MasterDataException;

/**
 * Interface CompanyManagerInterface
 * @package Officient\MasterData\Manager
 */
interface CompanyManagerInterface
{
    /**
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @return int
     * @throws MasterDataException
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int;

    /**
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return Company[]
     * @throws MasterDataException
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return Company|null
     * @throws MasterDataException
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?Company;

    /**
     * @param string $name
     * @param string $organizationNumber
     * @param string $address
     * @param string $zip
     * @param string $city
     * @param string $country
     * @param string $email
     * @param string $invoicingEmail
     * @param string $invoicingLanguage
     * @param int $paymentTerms
     * @param string $phone
     * @param \DateTimeInterface|null $freeTrialDatetime
     * @param string|null $domainTld
     * @param string|null $buyerReference
     * @param string|null $sellerReference
     * @param int|null $parentId
     * @param bool $active
     * @param int|null $v2FlowConfigId
     * @param string|null $endpointId
     * @param string|null $endpointIdType
     * @param string|null $invoicingDispatchType
     * @param string|null $orderReference
     * @param string|null $organizationNumberType
     * @param string|null $errorEmail
     * @return Company
     * @throws MasterDataException
     */
    public function store(
        string $name,
        string $organizationNumber,
        string $address, string $zip,
        string $city, string $country,
        string $email,
        string $invoicingEmail,
        string $invoicingLanguage,
        int $paymentTerms,
        string $phone,
        ?\DateTimeInterface $freeTrialDatetime = null,
        ?string $domainTld = null,
        ?string $buyerReference = null,
        ?string $sellerReference = null,
        ?int $parentId = null,
        bool $active = true,
        ?int $v2FlowConfigId = null,
        ?string $endpointId = null,
        ?string $endpointIdType = null,
        ?string $invoicingDispatchType = Company::INVOICING_DISPATCH_TYPE_EMAIL,
        ?string $orderReference = null,
        ?string $organizationNumberType = null,
        ?string $errorEmail = null
    ): Company;

    /**
     * @param Company $company
     * @return void
     * @throws MasterDataException
     */
    public function update(Company $company): void;

    /**
     * @param Company $company
     * @return void
     * @throws MasterDataException
     */
    public function destroy(Company $company): void;
}