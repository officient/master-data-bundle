<?php

namespace Officient\MasterData\Manager\Company;

use Officient\MasterData\Client;
use Officient\MasterData\ClientInterface;
use Officient\MasterData\Entity\Company;
use Officient\MasterData\Entity\User;
use Officient\MasterData\Exception\MasterDataException;
use Officient\MasterData\Exception\NonUniqueResultException;
use Officient\MasterData\Exception\NoResultException;
use Officient\MasterData\Factory\Company\NoteFactoryInterface;
use Officient\MasterData\Manager\AbstractManager;

class NoteManager extends AbstractManager implements NoteManagerInterface
{
    /**
     * @var NoteFactoryInterface
     */
    private $noteFactory;

    /**
     * @inheritDoc
     */
    public function __construct(ClientInterface $client, NoteFactoryInterface $noteFactory)
    {
        parent::__construct($client);
        $this->noteFactory = $noteFactory;
    }

    /**
     * @param Company $company
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @return int
     * @throws MasterDataException
     */
    public function countBy(Company $company, ?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $response = $this->client->doRequest('companies/'.$company->getId().'/notes/count_by', [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);
        return $response->getResult();
    }

    /**
     * @param Company $company
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     * @throws MasterDataException
     */
    public function findBy(Company $company, ?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $response = $this->client->doRequest('companies/'.$company->getId().'/notes/find_by', [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $result[] = $this->noteFactory->makeFromObject($record);
        }

        return $result;
    }

    /**
     * @param Company $company
     * @param array $criteria
     * @param array|null $orderBy
     * @return Company\Note|null
     * @throws MasterDataException
     */
    public function findOneBy(Company $company, array $criteria, ?array $orderBy = null): ?Company\Note
    {
        try {
            $response = $this->client->doRequest('companies/'.$company->getId().'/notes/find_one_by', [
                'criteria' => $criteria,
                'order_by' => $orderBy
            ], Client::METHOD_POST);
            $record = $response->getResult();
            return $this->noteFactory->makeFromObject($record);
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
    }

    /**
     * @param Company $company
     * @param string|null $note
     * @param $user
     * @return Company\Note
     * @throws MasterDataException
     */
    public function store(Company $company, ?string $note, $user): Company\Note
    {
        $userId = null;
        if($user instanceof User) {
            $userId = $user->getId();
        } else if(is_int($user)) {
            $userId = $user;
        }
        $data = [
            'note' => $note,
            'user_id' => $userId
        ];

        $response = $this->client->doRequest('companies/'.$company->getId().'/notes', $data, Client::METHOD_POST);
        $record = $response->getResult();

        return $this->noteFactory->makeFromObject($record);
    }

    /**
     * @param Company $company
     * @param Company\Note $note
     * @return Company\Note
     * @throws MasterDataException
     */
    public function update(Company $company, Company\Note $note): Company\Note
    {
        $data = [
            'note' => $note->getNote()
        ];

        $response = $this->client->doRequest('companies/'.$company->getId().'/notes/'.$note->getId(), $data, Client::METHOD_PATCH);
        $record = $response->getResult();

        return $this->noteFactory->makeFromObject($record);
    }

    /**
     * @param Company $company
     * @param Company\Note $note
     * @return void
     * @throws MasterDataException
     */
    public function destroy(Company $company, Company\Note $note): void
    {
        $this->client->doRequest('companies/'.$company->getId().'/notes/'.$note->getId(), null, Client::METHOD_DELETE);
    }
}