<?php

namespace Officient\MasterData\Manager\Company;

use Officient\MasterData\Entity\Company;
use Officient\MasterData\Entity\Company\Config;

/**
 * Interface ConfigManagerInterface
 * @package Officient\MasterData\Manager\Company
 */
interface ConfigManagerInterface
{
    /**
     * @param Company $company
     * @param string $identifier
     * @return Config|null
     */
    public function find(Company $company, string $identifier): ?Config;

    /**
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return Config[]
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param Company $company
     * @param string $identifier
     * @param string|null $default
     * @return string|null
     */
    public function findValue(Company $company, string $identifier, ?string $default = null): ?string;

    /**
     * @param Company $company
     * @param string $identifier
     * @param string $value
     * @return Config
     */
    public function store(Company $company, string $identifier, string $value): Config;

    /**
     * @param Company $company
     * @param string $identifier
     */
    public function destroy(Company $company, string $identifier): void;
}