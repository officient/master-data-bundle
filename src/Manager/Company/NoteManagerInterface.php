<?php

namespace Officient\MasterData\Manager\Company;

use Officient\MasterData\Entity\Company;

interface NoteManagerInterface
{
    /**
     * @param Company $company
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @return int
     */
    public function countBy(Company $company, ?array $criteria = null, ?int $limit = null, ?int $offset = null): int;

    /**
     * @param Company $company
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function findBy(Company $company, ?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param Company $company
     * @param array $criteria
     * @param array|null $orderBy
     * @return Company\Note|null
     */
    public function findOneBy(Company $company, array $criteria, ?array $orderBy = null): ?Company\Note;

    /**
     * @param Company $company
     * @param string|null $note
     * @param $user
     * @return Company\Note
     */
    public function store(Company $company, ?string $note, $user): Company\Note;

    /**
     * @param Company $company
     * @param Company\Note $note
     * @return Company\Note
     */
    public function update(Company $company, Company\Note $note): Company\Note;

    /**
     * @param Company $company
     * @param Company\Note $note
     * @return mixed
     */
    public function destroy(Company $company, Company\Note $note): void;
}