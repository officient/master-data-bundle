<?php

namespace Officient\MasterData\Manager\Company;

use Officient\MasterData\Client;
use Officient\MasterData\ClientInterface;
use Officient\MasterData\Entity\Company;
use Officient\MasterData\Entity\Company\Config;
use Officient\MasterData\Exception\NonUniqueResultException;
use Officient\MasterData\Exception\NoResultException;
use Officient\MasterData\Factory\Company\ConfigFactoryInterface;
use Officient\MasterData\Manager\AbstractManager;

class ConfigManager extends AbstractManager implements ConfigManagerInterface
{
    /**
     * @var ConfigFactoryInterface
     */
    private $configFactory;

    /**
     * @inheritDoc
     */
    public function __construct(ClientInterface $client, ConfigFactoryInterface $configFactory)
    {
        parent::__construct($client);
        $this->configFactory = $configFactory;
    }

    /**
     * @inheritDoc
     */
    public function find(Company $company, string $identifier): ?Config
    {
        try {
            $response = $this->client->doRequest("companies/{$company->getId()}/configs/{$identifier}");
            $record = $response->getResult();

            $config = new Config();
            $config->setIdentifier($identifier);
            $config->setValue($record->value);

            return $config;
        } catch (NoResultException | NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $response = $this->client->doRequest('companies/configs/find_by', [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $result[] = $this->configFactory->makeFromObject($record);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function findValue(Company $company, string $identifier, ?string $default = null): ?string
    {
        $config = $this->find($company, $identifier);
        return $config ? $config->getValue() : $default;
    }


    /**
     * @inheritDoc
     */
    public function store(Company $company, string $identifier, string $value): Config
    {
        $response = $this->client->doRequest("companies/{$company->getId()}/configs", [
            'identifier' => $identifier,
            'value' => $value
        ], Client::METHOD_POST);

        $record = $response->getResult();

        $config = new Config();
        $config->setIdentifier($identifier);
        $config->setValue($record->value);

        return $config;
    }

    /**
     * @inheritDoc
     */
    public function destroy(Company $company, string $identifier): void
    {
        $this->client->doRequest("companies/{$company->getId()}/configs/{$identifier}", null, Client::METHOD_DELETE);
    }
}