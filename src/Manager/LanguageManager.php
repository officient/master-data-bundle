<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Client;
use Officient\MasterData\ClientInterface;
use Officient\MasterData\Entity\Language;
use Officient\MasterData\Exception\NonUniqueResultException;
use Officient\MasterData\Exception\NoResultException;
use Officient\MasterData\Factory\LanguageFactoryInterface;

/**
 * Class LanguageManager
 * @package Officient\MasterData\Manager
 */
class LanguageManager extends AbstractManager implements LanguageManagerInterface
{
    /**
     * @var LanguageFactoryInterface
     */
    private $languageFactory;

    /**
     * @inheritDoc
     */
    public function __construct(ClientInterface $client, LanguageFactoryInterface $languageFactory)
    {
        parent::__construct($client);
        $this->languageFactory = $languageFactory;
    }


    /**
     * @inheritDoc
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $response = $this->client->doRequest('languages/count_by', [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);
        return $response->getResult();
    }

    /**
     * @inheritDoc
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $response = $this->client->doRequest('languages/find_by', [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $result[] = $this->languageFactory->makeFromObject($record);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?Language
    {
        try {
            $response = $this->client->doRequest('languages/find_one_by', [
                'criteria' => $criteria,
                'order_by' => $orderBy
            ], Client::METHOD_POST);
            $record = $response->getResult();

            return $this->languageFactory->makeFromObject($record);
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
    }
}