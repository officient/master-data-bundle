<?php

namespace Officient\MasterData\Manager\User;

use Officient\MasterData\Client;
use Officient\MasterData\Entity\User;
use Officient\MasterData\Entity\User\Config;
use Officient\MasterData\Exception\NonUniqueResultException;
use Officient\MasterData\Exception\NoResultException;
use Officient\MasterData\Manager\AbstractManager;

/**
 * Class ConfigManager
 * @package Officient\MasterData\Manager\User
 */
class ConfigManager extends AbstractManager implements ConfigManagerInterface
{
    /**
     * @inheritDoc
     */
    public function find(User $user, string $identifier): ?Config
    {
        try {
            $response = $this->client->doRequest("users/{$user->getId()}/configs/{$identifier}");
            $record = $response->getResult();

            $config = new Config();
            $config->setIdentifier($identifier);
            $config->setValue($record->value);

            return $config;
        } catch (NoResultException | NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function findValue(User $user, string $identifier, ?string $default = null): ?string
    {
        $config = $this->find($user, $identifier);
        return $config ? $config->getValue() : $default;
    }


    /**
     * @inheritDoc
     */
    public function store(User $user, string $identifier, string $value): Config
    {
        $response = $this->client->doRequest("users/{$user->getId()}/configs", [
            'identifier' => $identifier,
            'value' => $value
        ], Client::METHOD_POST);

        $record = $response->getResult();

        $config = new Config();
        $config->setIdentifier($identifier);
        $config->setValue($record->value);

        return $config;
    }

    /**
     * @inheritDoc
     */
    public function destroy(User $user, string $identifier): void
    {
        $this->client->doRequest("users/{$user->getId()}/configs/{$identifier}", null, Client::METHOD_DELETE);
    }
}