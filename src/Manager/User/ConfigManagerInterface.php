<?php

namespace Officient\MasterData\Manager\User;

use Officient\MasterData\Entity\User;
use Officient\MasterData\Entity\User\Config;

/**
 * Interface ConfigManagerInterface
 * @package Officient\MasterData\Manager\User
 */
interface ConfigManagerInterface
{
    /**
     * @param User $user
     * @param string $identifier
     * @return Config|null
     */
    public function find(User $user, string $identifier): ?Config;

    /**
     * @param User $user
     * @param string $identifier
     * @param string|null $default
     * @return string|null
     */
    public function findValue(User $user, string $identifier, ?string $default = null): ?string;

    /**
     * @param User $user
     * @param string $identifier
     * @param string $value
     * @return Config
     */
    public function store(User $user, string $identifier, string $value): Config;


    /**
     * @param User $user
     * @param string $identifier
     */
    public function destroy(User $user, string $identifier): void;

}