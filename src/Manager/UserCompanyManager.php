<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Client;
use Officient\MasterData\ClientInterface;
use Officient\MasterData\Entity\Company;
use Officient\MasterData\Entity\User;
use Officient\MasterData\Entity\UserCompany;
use Officient\MasterData\Exception\NonUniqueResultException;
use Officient\MasterData\Exception\NoResultException;
use Officient\MasterData\Factory\CompanyFactoryInterface;
use Officient\MasterData\Factory\UserCompanyFactoryInterface;
use Officient\MasterData\Factory\UserFactoryInterface;

/**
 * Class UserCompanyManager
 * @package Officient\MasterData\Manager
 */
class UserCompanyManager extends AbstractManager implements UserCompanyManagerInterface
{
    /**
     * @var UserCompanyFactoryInterface
     */
    private $userCompanyFactory;

    /**
     * @var UserFactoryInterface
     */
    private $userFactory;

    /**
     * @var CompanyFactoryInterface
     */
    private $companyFactory;

    /**
     * @inheritDoc
     */
    public function __construct(
        ClientInterface $client,
        UserCompanyFactoryInterface $userCompanyFactory,
        UserFactoryInterface $userFactory,
        CompanyFactoryInterface $companyFactory
    )
    {
        parent::__construct($client);
        $this->userCompanyFactory = $userCompanyFactory;
        $this->userFactory = $userFactory;
        $this->companyFactory = $companyFactory;
    }

    /**
     * @inheritDoc
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $response = $this->client->doRequest('user_companies/count_by', [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        return $response->getResult();
    }

    /**
     * @inheritDoc
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $response = $this->client->doRequest('user_companies/find_by', [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $record->user = $this->userFactory->makeFromObject($record->user);
            $record->company = $this->companyFactory->makeFromObject($record->company);

            $result[] = $this->userCompanyFactory->makeFromObject($record);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?UserCompany
    {
        try {
            $response = $this->client->doRequest('user_companies/find_one_by', [
                'criteria' => $criteria,
                'order_by' => $orderBy
            ], Client::METHOD_POST);
            $record = $response->getResult();

            $record->user = $this->userFactory->makeFromObject($record->user);
            $record->company = $this->companyFactory->makeFromObject($record->company);

            return $this->userCompanyFactory->makeFromObject($record);
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function store(User $user, Company $company, array $roles = []): UserCompany
    {
        $data = [
            'user_id' => $user->getId(),
            'company_id' => $company->getId(),
            'roles' => $roles
        ];
        $response = $this->client->doRequest('user_companies', $data, Client::METHOD_POST);
        $record = $response->getResult();

        $record->user = $this->userFactory->makeFromObject($record->user);
        $record->company = $this->companyFactory->makeFromObject($record->company);

        return $this->userCompanyFactory->makeFromObject($record);
    }

    /**
     * @inheritDoc
     */
    public function update(UserCompany $userCompany): void
    {
        $data = [
            'roles' => $userCompany->getRoles()
        ];

        $this->client->doRequest('user_companies/'.$userCompany->getId(), $data, Client::METHOD_PATCH);
    }

    /**
     * @inheritDoc
     */
    public function destroy(UserCompany $userCompany): void
    {
        $this->client->doRequest('user_companies/'.$userCompany->getId(), null, Client::METHOD_DELETE);
    }
}