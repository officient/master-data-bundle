<?php

namespace Officient\MasterData\Manager\Marketing;

/**
 * Interface SubscriptionManagerInterface
 * @package Officient\MasterData\Manager\Marketing
 */
interface SubscriptionManagerInterface
{
    /**
     * @param string $email
     */
    public function subscribe(string $email): void;

    /**
     * @param string $email
     */
    public function unsubscribe(string $email): void;
}