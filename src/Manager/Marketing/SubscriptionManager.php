<?php

namespace Officient\MasterData\Manager\Marketing;

use Officient\MasterData\Client;
use Officient\MasterData\Manager\AbstractManager;

/**
 * Class SubscriptionManager
 * @package Officient\MasterData\Manager\Marketing
 */
class SubscriptionManager extends AbstractManager implements SubscriptionManagerInterface
{
    /**
     * @inheritDoc
     */
    public function subscribe(string $email): void
    {
        $this->client->doRequest('marketing/subscriptions/'.$email, null, Client::METHOD_POST);
    }

    /**
     * @inheritDoc
     */
    public function unsubscribe(string $email): void
    {
        $this->client->doRequest('marketing/subscriptions/'.$email, null, Client::METHOD_DELETE);
    }
}