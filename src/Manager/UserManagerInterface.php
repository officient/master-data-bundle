<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Entity\User;
use Officient\MasterData\Exception\MasterDataException;

/**
 * Interface UserManagerInterface
 * @package Officient\MasterData\Manager
 */
interface UserManagerInterface
{
    /**
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @return int
     * @throws MasterDataException
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int;

    /**
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return User[]
     * @throws MasterDataException
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return User|null
     * @throws MasterDataException
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?User;

    /**
     * @param string|null $firstName
     * @param string|null $lastName
     * @param string $email
     * @param string|null $phone
     * @param array $roles
     * @param string|null $password
     * @param bool $emailConfirmed
     * @param bool $active
     * @param bool $sendEmail
     * @param string|null $sendEmailForwardUrl
     * @param string|null $locale
     * @param string|null $sendEmailAddress
     * @return User
     */
    public function store(
        ?string $firstName,
        ?string $lastName,
        string $email,
        ?string $phone,
        array $roles,
        string $password = null,
        bool $emailConfirmed = false,
        bool $active = false,
        bool $sendEmail = false,
        ?string $sendEmailForwardUrl = null,
        ?string $locale = null,
        ?string $sendEmailAddress = null
    ): User;

    /**
     * @param User $user
     * @return void
     * @throws MasterDataException
     */
    public function update(User $user): void;

    /**
     * @param User $user
     * @param string $password
     * @return void
     * @throws MasterDataException
     */
    public function updatePassword(User $user, string $password): void;

    /**
     * @param User $user
     * @param string $email
     * @param string|null $forwardUrl
     * @return void
     * @throws MasterDataException
     */
    public function updateEmail(User $user, string $email, ?string $forwardUrl = null): void;

    /**
     * @param User $user
     * @param string|null $forwardUrl
     * @return void
     */
    public function resendConfirmEmail(User $user, ?string $forwardUrl = null): void;

    /**
     * @param User $user
     * @return void
     * @throws MasterDataException
     */
    public function updateLastLogin(User $user): void;

    /**
     * @param User $user
     * @return void
     * @throws MasterDataException
     */
    public function destroy(User $user): void;
}