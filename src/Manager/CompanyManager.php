<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Client;
use Officient\MasterData\ClientInterface;
use Officient\MasterData\Entity\Company;
use Officient\MasterData\Exception\NonUniqueResultException;
use Officient\MasterData\Exception\NoResultException;
use Officient\MasterData\Factory\CompanyFactoryInterface;

/**
 * Class CompanyManager
 * @package Officient\MasterData\Manager
 */
class CompanyManager extends AbstractManager implements CompanyManagerInterface
{
    /**
     * @var CompanyFactoryInterface
     */
    private $companyFactory;

    /**
     * @inheritDoc
     */
    public function __construct(ClientInterface $client, CompanyFactoryInterface $companyFactory)
    {
        parent::__construct($client);
        $this->companyFactory = $companyFactory;
    }

    /**
     * @inheritDoc
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $response = $this->client->doRequest('companies/count_by', [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);
        return $response->getResult();
    }

    /**
     * @inheritDoc
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $response = $this->client->doRequest('companies/find_by', [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $result[] = $this->companyFactory->makeFromObject($record);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?Company
    {
        try {
            $response = $this->client->doRequest('companies/find_one_by', [
                'criteria' => $criteria,
                'order_by' => $orderBy
            ], Client::METHOD_POST);
            $record = $response->getResult();
            return $this->companyFactory->makeFromObject($record);
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function store(
        string $name,
        string $organizationNumber,
        string $address, string $zip,
        string $city, string $country,
        string $email,
        string $invoicingEmail,
        string $invoicingLanguage,
        int $paymentTerms,
        string $phone, ?\DateTimeInterface $freeTrialDatetime = null,
        ?string $domainTld = null,
        ?string $buyerReference = null,
        ?string $sellerReference = null,
        ?int $parentId = null,
        bool $active = true,
        ?int $v2FlowConfigId = null,
        ?string $endpointId = null,
        ?string $endpointIdType = null,
        ?string $invoicingDispatchType = Company::INVOICING_DISPATCH_TYPE_EMAIL,
        ?string $orderReference = null,
        ?string $organizationNumberType = null,
        ?string $errorEmail = null
    ): Company
    {
        $data = [
            'name' => $name,
            'organization_number' => $organizationNumber,
            'organization_number_type' => $organizationNumberType,
            'address' => $address,
            'zip' => $zip,
            'city' => $city,
            'country' => $country,
            'email' => $email,
            'invoicing_email' => $invoicingEmail,
            'invoicing_language' => $invoicingLanguage,
            'phone' => $phone,
            'buyer_reference' => $buyerReference,
            'seller_reference' => $sellerReference,
            'order_reference' => $orderReference,
            'payment_terms' => $paymentTerms,
            'active' => (int)$active
        ];
        if(!is_null($v2FlowConfigId)) {
            $data['v2_flow_config_id'] = $v2FlowConfigId;
        }
        if($freeTrialDatetime) {
            $data['free_trial_datetime'] = $freeTrialDatetime->format('Y-m-d H:i:s');
        }
        if($domainTld) {
            $data['domain_tld'] = $domainTld;
        }
        if($parentId) {
            $data['parent_id'] = $parentId;
        }
        if($endpointId) {
            $data['endpoint_id'] = $endpointId;
        }
        if($endpointIdType) {
            $data['endpoint_id_type'] = $endpointIdType;
        }
        if($invoicingDispatchType) {
            $data['invoicing_dispatch_type'] = $invoicingDispatchType;
        }
        if($errorEmail) {
            $data['error_email'] = $errorEmail;
        }
        $response = $this->client->doRequest('companies', $data, Client::METHOD_POST);
        $record = $response->getResult();

        return $this->companyFactory->makeFromObject($record);
    }

    /**
     * @inheritDoc
     */
    public function update(Company $company): void
    {
        $freeTrialDatetime = $company->getFreeTrialDatetime() ? $company->getFreeTrialDatetime()->format("Y-m-d H:i:s") : null;
        $data = [
            'name' => $company->getName(),
            'organization_number' => $company->getOrganizationNumber(),
            'organization_number_type' => $company->getOrganizationNumberType(),
            'address' => $company->getAddress(),
            'zip' => $company->getZip(),
            'city' => $company->getCity(),
            'country' => $company->getCountry(),
            'email' => $company->getEmail(),
            'invoicing_email' => $company->getInvoicingEmail(),
            'invoicing_language' => $company->getInvoicingLanguage(),
            'invoicing_dispatch_type' => $company->getInvoicingDispatchType(),
            'phone' => $company->getPhone(),
            'buyer_reference' => $company->getBuyerReference(),
            'seller_reference' => $company->getSellerReference(),
            'order_reference' => $company->getOrderReference(),
            'payment_terms' => $company->getPaymentTerms(),
            'free_trial_datetime' => $freeTrialDatetime,
            'parent_id' => $company->getParentId(),
            'active' => (int)$company->isActive(),
            'endpoint_id' => $company->getEndpointId(),
            'endpoint_id_type' => $company->getEndpointIdType(),
            'error_email' => $company->getErrorEmail(),
        ];

        $this->client->doRequest('companies/'.$company->getId(), $data, Client::METHOD_PATCH);
    }

    /**
     * @inheritDoc
     */
    public function destroy(Company $company): void
    {
        $this->client->doRequest('companies/'.$company->getId(), null, Client::METHOD_DELETE);
    }
}