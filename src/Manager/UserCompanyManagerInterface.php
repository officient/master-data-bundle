<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Entity\Company;
use Officient\MasterData\Entity\User;
use Officient\MasterData\Entity\UserCompany;
use Officient\MasterData\Exception\MasterDataException;

/**
 * Interface UserCompanyManagerInterface
 * @package Officient\MasterData\Manager
 */
interface UserCompanyManagerInterface
{
    /**
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @return int
     * @throws MasterDataException
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int;

    /**
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return UserCompany[]
     * @throws MasterDataException
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return UserCompany|null
     * @throws MasterDataException
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?UserCompany;

    /**
     * @param User $user
     * @param Company $company
     * @param array $roles
     * @return UserCompany
     * @throws MasterDataException
     */
    public function store(User $user, Company $company, array $roles = []): UserCompany;

    /**
     * @param UserCompany $userCompany
     * @return void
     * @throws MasterDataException
     */
    public function update(UserCompany $userCompany): void;

    /**
     * @param UserCompany $userCompany
     * @return void
     * @throws MasterDataException
     */
    public function destroy(UserCompany $userCompany): void;
}