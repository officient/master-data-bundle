<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Entity\Language;
use Officient\MasterData\Exception\MasterDataException;

/**
 * Interface LanguageManagerInterface
 * @package Officient\MasterData\Manager
 */
interface LanguageManagerInterface
{
    /**
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @return int
     * @throws MasterDataException
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int;

    /**
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return Language[]
     * @throws MasterDataException
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return Language|null
     * @throws MasterDataException
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?Language;
}