<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Client;
use Officient\MasterData\ClientInterface;
use Officient\MasterData\Entity\User;
use Officient\MasterData\Factory\UserFactoryInterface;

/**
 * Class AuthManager
 * @package Officient\MasterData\Manager
 */
class AuthManager extends AbstractManager implements AuthManagerInterface
{
    /**
     * @var UserFactoryInterface
     */
    private $userFactory;

    /**
     * @inheritDoc
     */
    public function __construct(ClientInterface $client, UserFactoryInterface $userFactory)
    {
        parent::__construct($client);
        $this->userFactory = $userFactory;
    }

    /**
     * @inheritDoc
     */
    public function authenticate(string $email, string $password): User
    {
        $data = [
            'email' => $email,
            'password' => $password
        ];
        $response = $this->client->doRequest('auth', $data, Client::METHOD_POST);
        $record = $response->getResult();

        return $this->userFactory->makeFromObject($record);
    }
}