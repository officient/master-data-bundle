<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\ClientInterface;
use Officient\MasterData\Entity\Company;
use Officient\MasterData\Exception\MasterDataException;

class AccessManager extends AbstractManager implements AccessManagerInterface
{
    /**
     * @inheritDoc
     * @throws MasterDataException
     */
    public function companyHasService(Company $company, string $serviceTag, bool $sendEmails = false): bool
    {
        $response = $this->client->doRequest('access/companies/'.$company->getId().'/has_service/'.$serviceTag.'?send_emails='.(int)$sendEmails, null, ClientInterface::METHOD_GET);
        return (bool)$response->getResult();
    }
}