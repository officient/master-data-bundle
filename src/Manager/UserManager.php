<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Client;
use Officient\MasterData\ClientInterface;
use Officient\MasterData\Entity\User;
use Officient\MasterData\Exception\MasterDataException;
use Officient\MasterData\Exception\NonUniqueResultException;
use Officient\MasterData\Exception\NoResultException;
use Officient\MasterData\Factory\UserFactoryInterface;

/**
 * Class UserManager
 * @package Officient\MasterData\Manager
 */
class UserManager extends AbstractManager implements UserManagerInterface
{
    /**
     * @var UserFactoryInterface
     */
    private $userFactory;

    /**
     * @inheritDoc
     * @param UserFactoryInterface $userFactory
     */
    public function __construct(ClientInterface $client, UserFactoryInterface $userFactory)
    {
        parent::__construct($client);
        $this->userFactory = $userFactory;
    }

    /**
     * @inheritDoc
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $response = $this->client->doRequest('users/count_by', [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        return $response->getResult();
    }

    /**
     * @inheritDoc
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $response = $this->client->doRequest('users/find_by', [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $result[] = $this->userFactory->makeFromObject($record);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?User
    {
        try {
            $response = $this->client->doRequest('users/find_one_by', [
                'criteria' => $criteria,
                'order_by' => $orderBy
            ], Client::METHOD_POST);
            $record = $response->getResult();

            return $this->userFactory->makeFromObject($record);
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function store(
        ?string $firstName,
        ?string $lastName,
        string $email,
        ?string $phone,
        array $roles,
        ?string $password = null,
        bool $emailConfirmed = false,
        bool $active = false,
        bool $sendEmail = false,
        ?string $sendEmailForwardUrl = null,
        ?string $locale = null,
        ?string $sendEmailAddress = null
    ): User
    {
        $data = [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'phone' => $phone,
            'roles' => $roles,
            'email_confirmed' => (int)$emailConfirmed,
            'active' => (int)$active,
            'password' => $password
        ];

        if($sendEmail) {
            $data['send_email'] = 1;
        }

        if($sendEmailForwardUrl) {
            $data['send_email_forward_url'] = $sendEmailForwardUrl;
        }

        if($locale) {
            $data['locale'] = $locale;
        }

        if($sendEmailAddress) {
            $data['send_email_address'] = $sendEmailAddress;
        }

        $response = $this->client->doRequest('users', $data, Client::METHOD_POST);
        $record = $response->getResult();

        return $this->userFactory->makeFromObject($record);
    }

    /**
     * @inheritDoc
     */
    public function update(User $user): void
    {
        $data = [
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'email' => $user->getEmail(),
            'phone' => $user->getPhone(),
            'roles' => $user->getRoles(),
            'email_confirmed' => (int)$user->isEmailConfirmed(),
            'active' => (int)$user->isActive()
        ];

        $this->client->doRequest('users/'.$user->getId(), $data, Client::METHOD_PATCH);
    }

    /**
     * @inheritDoc
     */
    public function updatePassword(User $user, string $password): void
    {
        $data = [
            'password' => $password
        ];

        $this->client->doRequest('users/'.$user->getId().'/password', $data, Client::METHOD_PATCH);
    }

    /**
     * @inheritDoc
     */
    public function updateEmail(User $user, string $email, ?string $forwardUrl = null): void
    {
        $data = [
            'email' => $email
        ];

        if($forwardUrl) {
            $data['forward_url'] = $forwardUrl;
        }

        $this->client->doRequest('users/'.$user->getId().'/email', $data, Client::METHOD_PATCH);
    }

    /**
     * @inheritDoc
     */
    public function resendConfirmEmail(User $user, ?string $forwardUrl = null): void
    {
        $data = [];

        if($forwardUrl) {
            $data['forward_url'] = $forwardUrl;
        }

        $this->client->doRequest('users/'.$user->getId().'/resend_confirm_email', $data, Client::METHOD_POST);
    }

    /**
     * @inheritDoc
     */
    public function updateLastLogin(User $user): void
    {
        $this->client->doRequest('users/'.$user->getId().'/update_last_login', null, Client::METHOD_PATCH);
    }

    /**
     * @inheritDoc
     */
    public function destroy(User $user): void
    {
        $this->client->doRequest('users/'.$user->getId(), null, Client::METHOD_DELETE);
    }
}