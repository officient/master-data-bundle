<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\ClientInterface;

/**
 * Class AbstractManager
 *
 * All managers should extend this class
 * A manager should handle the data flow between
 * the application and Master Data.
 *
 * @package Officient\MasterData\Manager
 */
abstract class AbstractManager
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * Manager constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Construct a GET query string
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return string
     */
    protected function constructQuery(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): string
    {
        $queryArr = array();

        if(!is_null($criteria)) {
            /*foreach($criteria as $key => $value) {
                $queryArr[] = $key."=".$value;
            }*/

            $queryArr[] = 'criteria='.json_encode($criteria);
        }

        if(!is_null($orderBy)) {
            /*$orderByArr = array();
            foreach($orderBy as $key => $value) {
                $orderByArr[] = $key."|".$value;
            }

            if(!empty($orderByArr)) {
                $queryArr[] = 'order_by='.implode(",", $orderByArr);
            }*/

            $queryArr[] = 'order_by='.json_encode($orderBy);
        }

        if(!is_null($limit)) {
            $queryArr[] = "limit=".$limit;
        }

        if(!is_null($offset)) {
            $queryArr[] = "offset=".$offset;
        }

        $query = "";
        if(!empty($queryArr)) {
            $query = "?".implode("&", $queryArr);
        }

        return $query;
    }
}