<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Entity\Company;

interface AccessManagerInterface
{
    /**
     * @param Company $company
     * @param string $serviceTag
     * @param bool $sendEmails
     * @return bool
     */
    public function companyHasService(Company $company, string $serviceTag, bool $sendEmails = false): bool;
}