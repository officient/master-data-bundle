<?php

namespace Officient\MasterData\Manager;

use Officient\MasterData\Entity\User;
use Officient\MasterData\Exception\AuthenticationFailedException;

/**
 * Interface AuthManagerInterface
 * @package Officient\MasterData\Manager
 */
interface AuthManagerInterface
{
    /**
     * @param string $email
     * @param string $password
     * @return User
     * @throws AuthenticationFailedException
     */
    public function authenticate(string $email, string $password): User;
}