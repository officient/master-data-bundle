<?php

namespace Officient\MasterData;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class MasterDataBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\MasterData
 */
class MasterDataBundle extends Bundle
{

}