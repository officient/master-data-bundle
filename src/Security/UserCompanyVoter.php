<?php

namespace Officient\MasterData\Security;

use Officient\MasterData\Entity\Company;
use Officient\MasterData\Entity\User;
use Officient\MasterData\Entity\UserCompany;
use Officient\MasterData\Manager\UserCompanyManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class UserCompanyVoter
 *
 * This class describes how the security layer should vote on a
 * UserCompany object. When passing a UserCompany object to second argument
 * on the method isGranted, this voter will be used.
 *
 * In short, it checks if a user has the a given role on a company
 * relation without having to get the relation in the process.
 *
 * @package Officient\MasterData\Security
 */
class UserCompanyVoter extends Voter
{
    /**
     * @inheritDoc
     */
    protected function supports($attribute, $subject)
    {
        if(!$subject instanceof UserCompany) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $roles = [];
        if(is_array($attribute)) {
            $roles = (array)$attribute;
        } else if(is_string($attribute)) {
            $roles = [$attribute];
        }

        $user = $token->getUser();

        if(!$user instanceof User) {
            return false;
        }

        $userCompany = $subject;

        if( empty(array_diff($roles, $userCompany->getRoles())) ) {
            return true;
        }

        return false;
    }
}