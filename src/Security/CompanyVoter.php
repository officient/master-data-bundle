<?php

namespace Officient\MasterData\Security;

use Officient\MasterData\Entity\Company;
use Officient\MasterData\Entity\User;
use Officient\MasterData\Manager\UserCompanyManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class CompanyVoter
 *
 * This class describes how the security layer should vote on a
 * Company object. When passing a Company object to second argument
 * on the method isGranted, this voter will be used.
 *
 * In short, it checks if a user has the a given role on a company
 * relation.
 *
 * @package Officient\MasterData\Security
 */
class CompanyVoter extends Voter
{
    /**
     * @var UserCompanyManagerInterface
     */
    private $userCompanyManager;

    /**
     * UserCompanyVoter constructor.
     * @param UserCompanyManagerInterface $userCompanyManager
     */
    public function __construct(UserCompanyManagerInterface $userCompanyManager)
    {
        $this->userCompanyManager = $userCompanyManager;
    }

    /**
     * @inheritDoc
     */
    protected function supports($attribute, $subject)
    {
        if(!$subject instanceof Company) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $roles = [];
        if(is_array($attribute)) {
            $roles = (array)$attribute;
        } else if(is_string($attribute)) {
            $roles = [$attribute];
        }

        $user = $token->getUser();

        if(!$user instanceof User) {
            return false;
        }

        $company = $subject;

        $relations = $this->userCompanyManager->findBy([
            'userId' => (string)$user->getId(),
            'companyId' => (string)$company->getId()
        ]);

        foreach($relations as $relation) {
            if( empty(array_diff($roles, $relation->getRoles())) ) {
                return true;
            }
        }

        return false;
    }
}