<?php

namespace Officient\MasterData;

use Officient\MasterData\Exception\AlreadyExistsException;
use Officient\MasterData\Exception\AuthenticationFailedException;
use Officient\MasterData\Exception\MasterDataException;
use Officient\MasterData\Exception\NonUniqueResultException;
use Officient\MasterData\Exception\NoResultException;
use Officient\MasterData\Exception\NotFoundException;
use Officient\MasterData\Exception\ValidationException;

/**
 * Class Response
 *
 * This class handles the Master Data response data
 *
 * @package Officient\MasterData
 */
class Response
{
    const CODE_OK                   = 1000;
    const CODE_NOT_FOUND            = 1001;
    const CODE_VALIDATION_ERROR     = 1002;
    const CODE_ALREADY_EXISTS       = 1003;
    const CODE_NON_UNIQUE_RESULT    = 1004;
    const CODE_NO_RESULT            = 1005;
    const CODE_EXCEPTION            = 1006;
    const CODE_ERROR                = 1007;

    const CODE_AUTH_FAILED          = 2000;

    /**
     * @var object
     */
    private $content;

    /**
     * Response constructor.
     * @param string $content
     * @throws MasterDataException
     */
    public function __construct(string $content)
    {
        $this->content = json_decode($content);
        $this->processCode();
    }

    /**
     * @throws MasterDataException
     */
    private function processCode()
    {
        switch($this->getCode()) {
            case self::CODE_NOT_FOUND:
                throw new NotFoundException($this->getMessage(), $this->getCode(), null, $this);
            case self::CODE_VALIDATION_ERROR:
                throw new ValidationException($this->getMessage(), $this->getCode(), null, $this);
            case self::CODE_ALREADY_EXISTS:
                throw new AlreadyExistsException($this->getMessage(), $this->getCode(), null, $this);
            case self::CODE_NON_UNIQUE_RESULT:
                throw new NonUniqueResultException($this->getMessage(), $this->getCode(), null, $this);
            case self::CODE_NO_RESULT:
                throw new NoResultException($this->getMessage(), $this->getCode(), null, $this);
            case self::CODE_AUTH_FAILED:
                throw new AuthenticationFailedException($this->getMessage(), $this->getCode(), null, $this);
            case self::CODE_EXCEPTION:
                $result = $this->getResult();
                $message = $this->getMessage()."\r\n";
                $message .= $result->namespace."\\".$result->exception."\r\n";
                $message .= $result->file." | Line ".$result->line."\r\n";
                $message .= $result->message;
                throw new MasterDataException($message, $this->getCode(), null, $this);
            case self::CODE_ERROR:
                throw new MasterDataException($this->getMessage(), $this->getCode(), null, $this);
        }
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return isset($this->content->message) ? $this->content->message : null;
    }

    /**
     * @return int|null
     */
    public function getCode(): ?int
    {
        return isset($this->content->code) ? $this->content->code : null;
    }

    /**
     * @return mixed|null
     */
    public function getResult()
    {
        return isset($this->content->result) ? $this->content->result : null;
    }
}