<?php

namespace Officient\MasterData\Factory\Company;

use Officient\MasterData\Entity\Company\Config;
use Officient\MasterData\Factory\AbstractFactory;

class ConfigFactory extends AbstractFactory implements ConfigFactoryInterface
{
    /**
     * @return string
     * @throws \ReflectionException
     */
    protected function getClassName(): string
    {
        return Config::class;
    }

}