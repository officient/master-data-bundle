<?php

namespace Officient\MasterData\Factory\Company;

use Officient\MasterData\Entity\Company\Note;
use Officient\MasterData\Factory\AbstractFactory;

class NoteFactory extends AbstractFactory implements NoteFactoryInterface
{
    /**
     * @return string
     * @throws \ReflectionException
     */
    protected function getClassName(): string
    {
        return Note::class;
    }

}