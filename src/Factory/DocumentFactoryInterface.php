<?php

namespace Officient\MasterData\Factory;

/**
 * Interface DocumentFactoryInterface
 * @package Officient\MasterData\Factory
 */
interface DocumentFactoryInterface extends FactoryInterface
{
    //
}