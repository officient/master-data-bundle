<?php

namespace Officient\MasterData\Factory;

/**
 * Interface ProductFactoryInterface
 * @package Officient\MasterData\Factory
 */
interface ProductFactoryInterface extends FactoryInterface
{
    //
}