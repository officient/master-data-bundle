<?php

namespace Officient\MasterData\Factory;

use stdClass;

/**
 * Interface FactoryInterface
 * @package Officient\MasterData\Factory
 */
interface FactoryInterface
{
    /**
     * @param stdClass $object
     * @return mixed
     */
    public function makeFromObject(stdClass $object);
}