<?php

namespace Officient\MasterData\Factory;

/**
 * Interface CompanyFactoryInterface
 * @package Officient\MasterData\Factory
 */
interface CompanyFactoryInterface extends FactoryInterface
{
    //
}