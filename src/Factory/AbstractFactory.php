<?php

namespace Officient\MasterData\Factory;

use stdClass;

/**
 * Class AbstractFactory
 * @package Officient\MasterData\Factory
 */
abstract class AbstractFactory implements FactoryInterface
{
    /**
     * @param stdClass $object
     * @return mixed
     * @throws \ReflectionException
     */
    public function makeFromObject(stdClass $object)
    {
        $className = $this->getClassName();
        $instance = new $className();
        $reflection = new \ReflectionObject($instance);

        foreach($object as $key => $value) {
            $value = $this->convertValue($value);
            $method = 'set'.ucfirst($key);

            if($reflection->hasMethod($method)) {
                $instance->{$method}($value);
            }
        }

        return $instance;
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    protected function getClassName(): string
    {
        $reflection = new \ReflectionClass(get_called_class());
        $called = $reflection->getShortName();
        return "Officient\\MasterData\\Entity\\".str_replace("Factory", "", $called);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    private function convertValue($value)
    {
        // Datetime maybe?
        if(is_object($value)) {
            if(isset($value->date)) {
                if(isset($value->timezone)) {
                    try {
                        return new \DateTime($value->date, new \DateTimeZone($value->timezone));
                    } catch (\Exception $e) {
                        return $value;
                    }
                } else {
                    try {
                        return new \DateTime($value->date);
                    } catch (\Exception $e) {
                        return $value;
                    }
                }
            }
        }

        return $value;
    }
}