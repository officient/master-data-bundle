<?php

namespace Officient\MasterData\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * This class describes the configuration for this bundle
 *
 * @package Officient\MasterData\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('master_data');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('api')
                    ->children()
                        ->scalarNode('host')->isRequired()->end()
                        ->integerNode('port')->defaultNull()->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}